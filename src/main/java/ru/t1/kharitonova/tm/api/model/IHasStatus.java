package ru.t1.kharitonova.tm.api.model;

import org.jetbrains.annotations.Nullable;
import ru.t1.kharitonova.tm.enumerated.Status;

public interface IHasStatus {

    @Nullable
    Status getStatus();

    void setStatus(@Nullable Status status);

}
